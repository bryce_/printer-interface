﻿using System;
using System.Collections.Generic;
//using System.Drawing; // Allows use of the "Graphics" object class and related features
using System.IO; // Allows text file reading and writing
using System.Linq;
using System.Reflection; // Allows use of finding variables by variable_field name via the handy FieldInfo variable_field
//using System.Runtime.InteropServices; // Allows use of "DllImport" and "Marshal."
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
//using System.Windows.Forms;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.IO.Ports;

namespace Independent_Printing_Interface {
	public partial class MainWindow:Window {
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Sourced Event Handlers
        public Threads_TCP threads_tcp;
        public Threads_Serial threads_serial;
        Adan _A = new Adan();

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Constants - Global const (instead of #define)
        const double PrintOffsetDelayCoefficient = 1.04; // actual ms per digital ms.  Multiplies the delay used by the travel delay equation:   actual delay = printer delay + PrintOffsetDelayCoefficient * travel delay
        const string password = "80085";
		const double RowCount = 5; // Number of rows the printer allows
		const double PixMM = 3.7795275591; //pixels per mm.  Ratio of pixels on-screen to mm in real life, with a standard monitor (1:1 scale)
		public static DateTime Today = DateTime.Today; // The full format of of the user's computer, including some mostly-unused formats
		public static List<string> DMY = new List<string> { DateTime.Today.ToString("d").Split(Convert.ToChar("/"))[0], DateTime.Today.ToString("d").Split(Convert.ToChar("/"))[1], DateTime.Today.ToString("d").Split(Convert.ToChar("/"))[2].Remove(0, 2) }; //[dd,mm,yy].  The current date of the user's computer
		const string status_blue = "#FFA3E6FF";
		const string status_red = "#FFFF6D42";
		const string status_green = "#FF57FF5F";

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Variables - Global dynamic
		double base_ruler_125sizeW; // pixels.  Size of the preivew ruler's grid at the designed 125mm width
		double base_ruler_125sizeH; // pixels.  Size of the preview ruler's grid at the designed 125mm height
		double print_area_size; // pixels.  Size of the print area containing the user input row text boxes

		double screen_width_ratio; // virtual pixels : real pixels.  Ratio of the virtual screen (based off resolution) to that of the real screen
		double screen_height_ratio;  // virtual pixels : real pixels.  Ratio of the virtual screen (based off resolution) to that of the real screen
		double base_window_minwidth; // virtual pixels.  Theoretical minimum width of the main new_window before scaling is applied
		double base_window_minheight; // virtual pixels.  Theoretical minimum height of the main new_window before scaling is applied
		static double base_window_width; // virtual pixels.  Theoretical width of the main new_window before scaling is applied
		static double base_window_height; // virtual pixels.  Theoretical height of the main new_window before scaling is applied

		double base_text_sidemargin; // pixels
		double base_grid_B_125sidemargin; // pixels
		double base_preview_offset_left; // pixels
		double base_preview_offset_right; // pixels
		static int justified_signum = 0; // -1, 0, 1.  Signum to represent the row text alignment

		double base_disabled_opacity; // #.  Value of opacity applied to items that are disabled and 'whited out'
		GridLength base_rows_size; // Default size of rows in GridLength format
		double base_font_size; // pixels.  Default size of the font
		static double rows_chosen = 5; // #.  Number of rows chosen by the user to print

		List<RowDefinition> rows_grid_I;
		List<RowDefinition> rows_grid_A;
		List<ColumnDefinition> rows_grid_B;
		List<TextBox> rows_text_I;
		List<TextBlock> rows_text_A;
		List<TextBlock> rows_text_B;

		string local_root = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\Project Files\Print Profiles\";
		//string optional_root = @"G:\Team Drives\Engineering\R&D\Resources\Adan's Testing Stuff\";
		string system_filename = "System Settings";
		string prior_filename_title = "Previous File";
		static string prior_filename = "";
		static string system_extension = ".log";
		static string profile_filename = "";
		//static string profile_extension = ".adp013"; // This also works and might make things relatively future-proof?
		static string profile_extension = ".txt";
		object system_data;
		object prior_filename_data;
		object profile_data;

		public MainWindow() {
			InitializeComponent();
            threads_serial = new Threads_Serial();
            threads_tcp = new Threads_TCP();
            Globals.running = true;

            // Record some referenced values that are established in the pre-modified user interface
            base_ruler_125sizeW = Preview_SizeW.Width.Value;
			base_ruler_125sizeH = Preview_SizeH.Height.Value;

			base_text_sidemargin = Text_Area.Margin.Left;
			base_disabled_opacity = Load_From_Printer_Grid.Opacity;
			base_preview_offset_left = Offset_Left_A.Width.Value;
			base_preview_offset_right = Offset_Right_A.Width.Value;
			base_window_minwidth = Scale_Window_Actual.MinWidth;
			base_window_minheight = Scale_Window_Actual.MinHeight;

			rows_grid_I = new List<RowDefinition> { Row_I1_Grid, Row_I2_Grid, Row_I3_Grid, Row_I4_Grid, Row_I5_Grid };
			rows_grid_A = new List<RowDefinition> { Row_A1_Grid, Row_A2_Grid, Row_A3_Grid, Row_A4_Grid, Row_A5_Grid };
			rows_grid_B = new List<ColumnDefinition> { Row_B1_Grid, Row_B2_Grid, Row_B3_Grid, Row_B4_Grid, Row_B5_Grid };
			base_rows_size = rows_grid_I[0].Height;

			rows_text_I = new List<TextBox> { Row_I1_Text, Row_I2_Text, Row_I3_Text, Row_I4_Text, Row_I5_Text };
			rows_text_A = new List<TextBlock> { Row_A1_Text, Row_A2_Text, Row_A3_Text, Row_A4_Text, Row_A5_Text };
			rows_text_B = new List<TextBlock> { Row_B1_Text, Row_B2_Text, Row_B3_Text, Row_B4_Text, Row_B5_Text };
			base_font_size = rows_text_I[0].FontSize;
			base_grid_B_125sidemargin = rows_text_B[0].Margin.Left;

			system_data = _A.Create_DataList(
                Selected_Root,
				_A.Data(Printing_Speed, "   Printing Speed (mm/s)"),
				_A.Data(Printing_Space, "   Printing Length (mm)"),
				_A.Data(Actuation_Delay, "   Actuation Delay (ms)"),
				_A.Data(Printer_Delay, "   Printer Delay (ms)"),
				_A.Data(Printer_ID, "   Printer ID"),
				_A.Data(COM_Port, "   COM Port"),
				_A.Data(Screen_Width, "   Screen Width (mm)"),
				_A.Data(Screen_Height, "   Screen Height (mm)"),
				_A.Data(_A.PtrCrt(this, nameof(base_window_width)), "   Digital Width (pixels)"),
				_A.Data(_A.PtrCrt(this, nameof(base_window_height)), "   Digital Height (pixels)")
				);

			prior_filename_data = _A.Create_DataList(
				_A.PtrCrt(this, nameof(prior_filename))
				);

			profile_data = _A.Create_DataList(
				_A.PtrCrt(this, nameof(profile_filename)),
				_A.Data(rows_text_I[0], "   (Row 1 text)"),
				_A.Data(rows_text_I[1], "   (Row 2 text)"),
				_A.Data(rows_text_I[2], "   (Row 3 text)"),
				_A.Data(rows_text_I[3], "   (Row 4 text)"),
				_A.Data(rows_text_I[4], "   (Row 5 text)"),
				_A.Data(_A.List_Children(Selected_Row_Grid, typeof(RadioButton)), "   Active Row"),
				_A.Data(Loaded_File_Name, "   Loaded file name (from printer)"),
				_A.Data(_A.List_Children(Selected_Alignment_Grid, typeof(RadioButton)), "   Text Alignment"),
				_A.Data(Print_Offset, "   Print Offset (mm)")
				);

			// Initialise the modified user interface components
			Day.Text = DMY[0];
			Month.Text = DMY[1];
			Year.Text = DMY[2];
			DMY_text.Text = DMY[0] + " " + DMY[1] + " " + DMY[2];

			_A.BaseBackgrounds[""] = Grey_Button.Background;
			_A.Disable_BorderButton(Disconnect_Button);
			_A.Disable_BorderButton(Home_Button);
			_A.Disable_BorderButton(Print_Button);
			Update_Magnification();

			Load_System();
			Load_ListBox(local_root, Local_Profiles);
			//Load_ListBox(Selected_Root.Text, Optional_Profiles);
			Load_PriorFile();
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//   USE-CASE FILE OPERATIONS
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Ask the user for the password.  Return true if correct and false if incorrect
		private bool Password_Request() {
			string password_input = _A.Window_Waiter(this, new TextboxWindow("Please enter the password"), "text_input") as string; // Ask for user password

			if (password_input == null) { // Cancel was clicked
				return false;
			} else if (password_input != password) { // The wrong password was entered
				MessageBox.Show("That was the wrong password!");
				return false;
			} else { // The correct password was entered
				return true;
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Save the system settings to the local computer directory
		private void Save_System(object sender = null, MouseButtonEventArgs e = null) {
			System.IO.Directory.CreateDirectory(local_root); // Ensures the local directory exists by simply creating it if it doesn't yet exist

			if (sender != null) { // The button was clicked, and so it's considered an overwrite of system files
				if (Password_Request() == false) { return; } // Only continue if the correct password was input
			}

			_A.Save_Data(local_root + system_filename + system_extension, system_data);
			MessageBox.Show("Successfully saved the system settings to the local directory.   " + local_root + system_filename + system_extension);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Save the system settings to the local computer directory
		private void Load_System(object sender = null, MouseButtonEventArgs e = null) {
			System.IO.Directory.CreateDirectory(local_root); // Ensures the local directory exists by simply creating it if it doesn't yet exist

			if (!File.Exists(local_root + system_filename + system_extension)) {
				MessageBox.Show("No system settings had previously been saved.  Creating a restoration point now with default settings.");
				Save_System();
			} else {
				_A.Load_Data(local_root + system_filename + system_extension, system_data);
			}

			Scale_Screen();
			Update_Print_Area_Size();
            Load_ListBox(Selected_Root.Text, Optional_Profiles);
        }

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Save the	most recent active filename
		private void Save_PriorFile(bool local = true) {
			System.IO.Directory.CreateDirectory(local_root); // Ensures the local directory exists by simply creating it if it doesn't yet exist
			if (local == true) { // Only save the previous file if it's local, to ensure we can access it since optional profiles aren't necessarily reliable
				prior_filename = profile_filename;
				_A.Save_Data(local_root + prior_filename_title + system_extension, prior_filename_data); // Save the file name so that it auto-loads next time the system is turned on
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Save the	most recent active filename
		private void Load_PriorFile() {
			System.IO.Directory.CreateDirectory(local_root); // Ensures the local directory exists by simply creating it if it doesn't yet exist
			if (File.Exists(local_root + prior_filename_title + system_extension)) { // There is a Previous File saved to load profile_filename from
				_A.Load_Data(local_root + prior_filename_title + system_extension, prior_filename_data);
				if (prior_filename != "") { // There is a profile_filename to load profile_data from
					if (File.Exists(local_root + prior_filename + profile_extension)) {
						_A.Load_Data(local_root + prior_filename + profile_extension, profile_data);
					}
				}
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Save a profile to the specified address
		private void Save_Profile(bool local, string address) {
			if (!Directory.Exists(address)) { // Check if the directory is available and throw a failure message if not
				if (local == true) {
					MessageBox.Show("Couldn't access the local computer directory.\n\n" + local_root);
				} else {
					MessageBox.Show("Couldn't access the optional file directory.\n\n" + Selected_Root.Text);
				}
				return;
			}

			profile_filename = _A.Window_Waiter(this, new TextboxWindow("Please enter the new file name"), "text_input") as string; // Asks the user for a file name
			if (profile_filename == "" || profile_filename == null) { return; } // Either cancel was clicked or no text was entered

			address += profile_filename + profile_extension;
			if (File.Exists(address)) { // Check if the file already exists so that we don't accidentally overwrite it
				MessageBox.Show("'Save as New' unsuccessful because the new profile name matched a profile that already exists.   " + address); return;
			}

			_A.Save_Data(address, profile_data); // All is good, so proceed with the profile save
			if (local == true) {
				MessageBox.Show("Saved to local computer directory.\n\n" + local_root + profile_filename + profile_extension);
			} else {
				MessageBox.Show("Saved to optional file directory.\n\n" + Selected_Root.Text + profile_filename + profile_extension);
			}
			Save_PriorFile(local);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Save a profile to the local computer directory
		private void Save_Profile_Local(object sender, MouseButtonEventArgs e) {
			System.IO.Directory.CreateDirectory(local_root); // Ensures the local directory exists by simply creating it if it doesn't yet exist
			Save_Profile(true, local_root);
			Load_ListBox(local_root, Local_Profiles);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Save a profile to the optional file directory
		private void Save_Profile_Optional(object sender, MouseButtonEventArgs e) {
			Save_Profile(false, Selected_Root.Text);
			Load_ListBox(Selected_Root.Text, Optional_Profiles);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Overwrite the focused profile
		// NB: No need to check if directory is accessible, since you can't click on something that isn't there in the first place
		private void Overwrite_Profile(object sender, MouseButtonEventArgs e) {
			ListBoxItem profile = _A.Focused_ListBoxItem(Local_Profiles.Items, Optional_Profiles.Items);
			if (profile == null) { return; } // Nothing was actually clicked on
			profile_filename = profile.Content as string;

			if (Password_Request() == false) { return; }

			if (Local_Profiles == profile.Parent) {
				_A.Save_Data(local_root + profile_filename + profile_extension, profile_data);
				Save_PriorFile(true);
			} else if (Optional_Profiles == profile.Parent) {
				_A.Save_Data(Selected_Root.Text + profile_filename + profile_extension, profile_data);
				Save_PriorFile(false);
			}

			MessageBox.Show("Successfully overwritten the profile.\n\n" + profile_filename);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Load the focused profile
		// NB: No need to check if directory is accessible, since you can't click on something that isn't there in the first place
		private void Load_Profile(object sender, MouseButtonEventArgs e) {
			ListBoxItem profile = _A.Focused_ListBoxItem(Local_Profiles.Items, Optional_Profiles.Items);
			profile_filename = profile.Content as string;

			if (Local_Profiles == profile.Parent) {
				_A.Load_Data(local_root + profile_filename + profile_extension, profile_data);
				Save_PriorFile(true);
			} else if (Optional_Profiles == profile.Parent) {
				_A.Load_Data(Selected_Root.Text + profile_filename + profile_extension, profile_data);
				Save_PriorFile(false);
			}

            // Now update all fields to match the profile loading
			Update_Print_Area_Size();
            //Update_Row_Size();
            Row_Change();
            Update_Alignment();
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Overwrite the focused profile
		// NB: No need to check if directory is accessible, since you can't click on something that isn't there in the first place
		private void Delete_Profile(object sender, MouseButtonEventArgs e) {
			ListBoxItem profile = _A.Focused_ListBoxItem(Local_Profiles.Items, Optional_Profiles.Items);
			if (profile == null) { return; } // Nothing was actually clicked on
			profile_filename = profile.Content as string;

			if (Password_Request() == false) { return; }

			if (Local_Profiles == profile.Parent) {
				_A.Delete_Data(local_root + profile_filename + profile_extension);
				Load_ListBox(local_root, Local_Profiles);
			} else if (Optional_Profiles == profile.Parent) {
				_A.Delete_Data(Selected_Root.Text + profile_filename + profile_extension);
				Load_ListBox(Selected_Root.Text, Optional_Profiles);
			}

			if (profile_filename == prior_filename) { // If it's the prior file, delete the prior_filename to prevent issues
				prior_filename = "";
				Save_PriorFile();
			}

			MessageBox.Show("Successfully deleted the profile.\n\n" + profile_filename);
		}

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Load a list of all profiles in the chosen root path
        private void Load_ListBox(object sender, RoutedEventArgs e) { // Handler to change the path when the box loses focus
            if (Selected_Root.Text[Selected_Root.Text.Length-1] != '\\') {
                Selected_Root.Text += '\\';
            }
            Load_ListBox(Selected_Root.Text, Optional_Profiles);
        }

        private void Load_ListBox(string root, object box) { Load_ListBox(root, box as ListBox); }
		private void Load_ListBox(string root, ListBox box) {
			box.Items.Clear();
            if (root == "\\" || root == "") { return; } // Do nothing if the root was entered blank

			if (!Directory.Exists(root)) {
				if (root == local_root) {
					MessageBox.Show("Unable to access the local computer directory.\n\n" + root);
				} else if (root == Selected_Root.Text) {
					MessageBox.Show("Unable to access the optional file directory.\n\n" + root);
				} else {
					MessageBox.Show("Unable to access the directory.\n\n" + root);
				}
			} else {
                foreach (string profile in Directory.GetFiles(root, "*" + profile_extension)) {
                    box.Items.Add(new ListBoxItem { Content = profile.Split(Convert.ToChar(@"\")).Last().Replace(profile_extension, "") });
                }
            }
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//   USER INTERFACE
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Enables the horizontal scrollbar to be moved by mousewheel
		private void Horizontal_ScrollViewer_MouseWheel(object sender, MouseWheelEventArgs e) {
			(sender as ScrollViewer).ScrollToHorizontalOffset((sender as ScrollViewer).ContentHorizontalOffset - e.Delta);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Scale the entire main new_window based on the user-specified screen dimensions
		private void Scale_Screen(object sender = null, KeyEventArgs e = null) {
			if (_A.INum(Screen_Width) >= 70 && _A.INum(Screen_Height) >= 70) { // Only scale if the mm values are sufficiently large, since massive stretching is very resource-intensive
				screen_width_ratio = (508 / _A.INum(Screen_Width)) * (1920 / System.Windows.SystemParameters.WorkArea.Width);
				screen_height_ratio = (285.75 / _A.INum(Screen_Height)) * (1080 / System.Windows.SystemParameters.WorkArea.Height);
				Scale_Window_Grid.ScaleX = screen_width_ratio;
				Scale_Window_Grid.ScaleY = screen_height_ratio;
				Scale_Window_Actual.MinWidth = base_window_minwidth * screen_width_ratio;
				Scale_Window_Actual.MinHeight = base_window_minheight * screen_height_ratio;
				Scale_Window_Actual.Width = base_window_width * screen_width_ratio;
				Scale_Window_Actual.Height = base_window_height * screen_height_ratio;
			}
		}

		private void Main_Window_SizeChanged(object sender = null, SizeChangedEventArgs e = null) {
			base_window_width = Scale_Window_Actual.Width / screen_width_ratio;
			base_window_height = Scale_Window_Actual.Height / screen_height_ratio;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Update the magnification view of the printing area
		private void Update_Magnification(object sender = null, MouseEventArgs e = null) {
			Print_Magnifier.ScaleX = Magnification_Slider.Value;
			Print_Magnifier.ScaleY = Magnification_Slider.Value;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Update the grid size based on the printing space limits
		private void Update_Print_Area_Size(object sender = null, KeyEventArgs e = null) {
			Text_Area.Margin = new Thickness(base_text_sidemargin + _A.IfCalc(justified_signum*-1, PixMM * _A.INum(Print_Offset)), 0, base_text_sidemargin + _A.IfCalc(justified_signum, PixMM * _A.INum(Print_Offset)), 0);
			for (int i = 0; i < RowCount; i++) {
				rows_text_I[i].FontSize = base_font_size * (RowCount / rows_chosen);
			}

			double limit = _A.INum(Printing_Space);
			print_area_size = 2*base_text_sidemargin + limit * PixMM;
			Print_Area.Width = print_area_size;

			switch (justified_signum) { // Update the horizontal scrollbar location to match the text alignment
				case -1: Text_ScrollViewer.ScrollToHorizontalOffset(2 * PixMM * _A.INum(Print_Offset)); break;
				case 1: Text_ScrollViewer.ScrollToHorizontalOffset(print_area_size - 25 - 2 * PixMM * _A.INum(Print_Offset)); break;
				case 0: Text_ScrollViewer.ScrollToHorizontalOffset((print_area_size - 25)/2); break;
			}

			Row_B_Translate.Y = (justified_signum * _A.INum(Print_Offset) * PixMM)/2;
			for (int i = 0; i < RowCount; i++) { // Update the width of the vertical preview text by using new margins
				rows_text_B[i].Margin = new Thickness((125 - limit)/2 * PixMM + base_grid_B_125sidemargin);
			}

			Preview_SizeW.Width = new GridLength(-(125 - limit) * PixMM + base_ruler_125sizeW); // Update the preview ruler lengths
			Preview_SizeH.Height = new GridLength(-(125 - limit) * PixMM + base_ruler_125sizeH);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Change the number of printing rows and automatically adjust everything to match
		private void Update_Row_Size() {
			for (int i = 0; i < RowCount; i++) {
				rows_text_I[i].FontSize = base_font_size * (RowCount / rows_chosen);
				rows_text_A[i].FontSize = base_font_size * (RowCount / rows_chosen);
				rows_text_B[i].FontSize = base_font_size * (RowCount / rows_chosen);

				if (i < rows_chosen) {
					rows_grid_I[i].Height = base_rows_size;
					rows_grid_A[i].Height = base_rows_size;
					rows_grid_B[i].Width = base_rows_size;
					rows_text_B[i].Opacity = 1;
				} else {
					rows_grid_I[i].Height = new GridLength(0);
					rows_grid_A[i].Height = new GridLength(0);
					rows_grid_B[i].Width = new GridLength(0);
					rows_text_B[i].Opacity = 0;
				}
			}
		}

		private void Row_Change(object sender = null, RoutedEventArgs e = null) {

            Selected_Alignment_Grid.Opacity = 1; // Enable the offset
            Selected_Alignment_Grid.IsEnabled = true;

            if (_1_Rows.IsChecked == true) {
				rows_chosen = 1;
			} else if (_2_Rows.IsChecked == true) {
				rows_chosen = 2;
			} else if (_3_Rows.IsChecked == true) {
				rows_chosen = 3;
			} else if (_4_Rows.IsChecked == true) {
				rows_chosen = 4;
			} else if (_5_Rows.IsChecked == true) {
				rows_chosen = 5;
			} else if (Directly_From_Printer.IsChecked == true) { // Loading data from the printer's files instead of the user's files
				Load_From_Printer_Grid.IsEnabled = false;
				Load_From_Printer_Grid.Opacity = base_disabled_opacity;
				Text_ScrollViewer.IsEnabled = false;
				Text_ScrollViewer.Opacity = base_disabled_opacity;
                Justified_Grid.Opacity = 1; // Enable the offset
                Justified_Grid.IsEnabled = true;
                Left_Align.IsChecked = true;
                Selected_Alignment_Grid.Opacity = base_disabled_opacity; // Disable the alignment grid
                Selected_Alignment_Grid.IsEnabled = false;
                justified_signum = -1;
                return; // Don't continue the function, since we're loading from printer instead
			} else if (FileLoad_From_Printer.IsChecked == true) { // Loading data from the printer's files instead of the user's files
				Load_From_Printer_Grid.IsEnabled = true;
				Load_From_Printer_Grid.Opacity = 1;
				Text_ScrollViewer.IsEnabled = false;
				Text_ScrollViewer.Opacity = base_disabled_opacity;
				return; // Don't continue the function, since we're loading from printer instead
			}

			Load_From_Printer_Grid.Opacity = base_disabled_opacity;
			Load_From_Printer_Grid.IsEnabled = false;
			Text_ScrollViewer.IsEnabled = true;
			Text_ScrollViewer.Opacity = 1;

			Update_Row_Size();
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Change the number of printing rows and automatically adjust everything to match
		private void Update_Alignment(object sender, KeyEventArgs e) { Update_Alignment(sender); } // Serves the numberbox itself
		private void Update_Alignment(object sender = null, RoutedEventArgs e = null) {
			if (Left_Align.IsChecked == true) {
				justified_signum = -1;
				Justified_Change(TextAlignment.Left);
			} else if (Right_Align.IsChecked == true) {
				justified_signum = 1;
				Justified_Change(TextAlignment.Right);
			} else if (Center_Align.IsChecked == true) {
				justified_signum = 0;
				Justified_Change(TextAlignment.Center);
			}

			if (justified_signum == 0) { // Disable the justified offset textbox if using Center
				Justified_Grid.Opacity = base_disabled_opacity;
				Justified_Grid.IsEnabled = false;
			} else {
				Justified_Grid.Opacity = 1;
				Justified_Grid.IsEnabled = true;
			}

			Offset_Right_A.Width = new GridLength(base_preview_offset_right + _A.IfCalc(justified_signum, PixMM * _A.INum(Print_Offset)));
			Offset_Right_B.Height = new GridLength(base_preview_offset_right + _A.IfCalc(justified_signum, PixMM * _A.INum(Print_Offset)));
			Offset_Left_A.Width = new GridLength(base_preview_offset_left + _A.IfCalc(justified_signum*-1, PixMM * _A.INum(Print_Offset)));
			Offset_Left_B.Height = new GridLength(base_preview_offset_left + _A.IfCalc(justified_signum*-1, PixMM * _A.INum(Print_Offset)));

			Update_Print_Area_Size();
		}

		private void Justified_Change(TextAlignment new_alignment) {
			for (int i = 0; i < RowCount; i++) {
				rows_text_I[i].TextAlignment = new_alignment;
				rows_text_A[i].TextAlignment = new_alignment;
				rows_text_B[i].TextAlignment = new_alignment;
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Matches the contents of the preview-based textblocks to the contents of the input-based textboxes
		private void MatchTextBlocks(TextBox sender, List<TextBlock> receiver, int paste_size) {
			for (int i = 0; i < paste_size; i++) {
				receiver[i].Text = sender.Text;
			}
		}

		private void Update_RowContent(object sender, KeyEventArgs e) {
			if (sender.Equals(Row_I1_Text)) {
				MatchTextBlocks(Row_I1_Text, new List<TextBlock> { Row_A1_Text, Row_B1_Text }, 2);
			} else if (sender.Equals(Row_I2_Text)) {
				MatchTextBlocks(Row_I2_Text, new List<TextBlock> { Row_A2_Text, Row_B2_Text }, 2);
			} else if (sender.Equals(Row_I3_Text)) {
				MatchTextBlocks(Row_I3_Text, new List<TextBlock> { Row_A3_Text, Row_B3_Text }, 2);
			} else if (sender.Equals(Row_I4_Text)) {
				MatchTextBlocks(Row_I4_Text, new List<TextBlock> { Row_A4_Text, Row_B4_Text }, 2);
			} else if (sender.Equals(Row_I5_Text)) {
				MatchTextBlocks(Row_I5_Text, new List<TextBlock> { Row_A5_Text, Row_B5_Text }, 2);
			}
		}

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Prevents the window from closing if the system is connected
        private void Prevent_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (threads_tcp.running) {
                Disconnect();
            }
            /*if (threads_tcp.running) {
                MessageBox.Show("Unable to close this program while connected to the printer!  Please disconnect first.");
                e.Cancel = true;
            }*/
            /*if (threads_tcp.running) {
                var window = MessageBox.Show(
                "Close the window?",
                "Are you sure?",
                MessageBoxButton.YesNo);

                //e.Cancel = (window == DialogResult.No);
                //e.Cancel = true;
                e.Cancel = DialogResult.Yes;
            }*/
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //   RUNTIME OPERATIONS
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Checks for connection availability of both the Arduino and printer, and proceeds to establish connection with both
        private void Connect(object sender, MouseButtonEventArgs e) {

            if (!threads_tcp.running) {
                try { // 1. Check connection availability of printer and attempt connection
                    threads_tcp.receive = new Thread(threads_tcp.rec);
                    threads_tcp.process = new Thread(threads_tcp.proccess);
                    threads_tcp.running = true;
                    threads_tcp.tcp.ServerStart();
                    threads_tcp.receive.Start();
                    threads_tcp.process.Start();
                } catch (Exception){ // Printer can't be connected
				    Status_Text.Text = "Printer connection unavailable";
				    Status_Colour.Color = _A.ColourHash(status_red);
                    Disconnect();
				    return;
                }
                
                try { // 2. Check connection availability of Arduino and attempt connection
                    if(SerialPort.GetPortNames().Contains("COM" + COM_Port.Text) == false) {
                        throw new Exception();
                    }
                    Globals.portIn = new SerialPort("COM" + COM_Port.Text, 115200, Parity.None, 8, StopBits.One); //starts new threads
                    Globals.portIn.ReadTimeout = 500;
                    Globals.portIn.WriteTimeout = 500;

                    threads_serial = new Threads_Serial();
                    Globals.running = true;
                    Globals.readThread = new Thread(threads_serial.ReadSerial);
                    Globals.readThread.Start();
                    Globals.processThread = new Thread(threads_serial.processThread);
                    Globals.processThread.Start();
                }   catch (Exception){ // Arduino can't be connected
                //no_arduino:
                    Status_Text.Text = "Arduino connection unavailable";
                    Status_Colour.Color = _A.ColourHash(status_red);
                    Disconnect();
                    return;
                }
            }

			//<> 3. Attempt connection with printer
			//if (false) { // Printer connection failed
			//	Status_Text.Text = "Printer connection available, but connection itself has failed";
			//	Status_Colour.Color = _A.ColourHash(status_red);
			//	return;
			//}

			//<> 4. Attempt connection with Arduino
			//if (false) { // Arduino connection failed
			//	Status_Text.Text = "Arduino connection available, but connection itself has failed";
			//	Status_Colour.Color = _A.ColourHash(status_red);
			//	return;
			//}

			// All connections established correctly, so proceed with the function
			Status_Text.Text = "2. Please click \"HOME\" to prepare the motor";
			Status_Colour.Color = _A.ColourHash(status_blue);
			//Status_Colour.Color = _A.ColourHSLA(1.4, 0, 0.75);
			_A.Disable_BorderButton(Connect_Button);
			_A.Enable_BorderButton(Disconnect_Button);
			_A.Enable_BorderButton(Home_Button);
			_A.Disable_BorderButton(Print_Button);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Disconnects from both the Arduino and printer through a slow and secure method
		private void Disconnect(object sender = null, MouseButtonEventArgs e = null) {
            try { // Attempt disconnection of printer
                if (threads_tcp.running) {
                    threads_tcp.tcp.stream.Close();
                    threads_tcp.tcp.client.Close();
                    threads_tcp.running = false;
                    Globals.running = false;
                }
            } catch (Exception){ // Printer disconnection failed, so stop the function and disable the system
                if (sender != null) { // Only show the message if sent by the button itself
                    Status_Text.Text = "Printer disconnection failed somehow!";
                    Status_Colour.Color = _A.ColourHash(status_red);
                }
                _A.Disable_BorderButton(Home_Button);
                _A.Disable_BorderButton(Print_Button);
                return;
            }

			//<> Attempt disconnection of Arduino
			if (false) { // Arduino disconnection failed, so stop the function and disable the system
                Status_Text.Text = "Arduino disconnection failed somehow!";
                Status_Colour.Color = _A.ColourHash(status_red);
                _A.Disable_BorderButton(Home_Button);
                _A.Disable_BorderButton(Print_Button);
                return;
			}

			// All disconnections succeeded, so proceed with the function
            if (sender != null) { // Only show the message if sent by the button itself
                Status_Text.Text = "It is now safe to turn off your system";
                Status_Colour.Color = _A.ColourHash(status_blue);
            }
            _A.Enable_BorderButton(Connect_Button);
            _A.Disable_BorderButton(Disconnect_Button);
            _A.Disable_BorderButton(Home_Button);
            _A.Disable_BorderButton(Print_Button);
        }

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Disconnects from both the Arduino and printer through a brute-forced method
		private void EStop(object sender, MouseButtonEventArgs e) {
            //<> Perhaps immediately close the COM ports?
            Disconnect();
            /*Status_Text.Text = "It is now safe to turn off your system";
			Status_Colour.Color = _A.ColourHash(status_blue);
			_A.Enable_BorderButton(Connect_Button);
			_A.Disable_BorderButton(Disconnect_Button);
			_A.Disable_BorderButton(Home_Button);
			_A.Disable_BorderButton(Print_Button);*/
        }

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Sends a signal to the connected Arduino to home its actuator
		private void Home(object sender, MouseButtonEventArgs e) {
            threads_serial.send(1,0); // Signal to Arduino: Home function
            System.Threading.Thread.Sleep(1200); // Wait enough time for the homing function to end
            //<> Receive a message from the Arduino about whether or not the homing was successful
            if (false) { // Homing failed, so stop the function
				Status_Text.Text = "Homing was unsuccessful.  This might be for one of many reasons, so please inform your friendly neighbourhood Engineer!";
                MessageBox.Show(Status_Text.Text);
				Status_Colour.Color = _A.ColourHash(status_red);
				return;
			}

			// Homing succeeded, so proceed with the function
			Status_Text.Text = "All systems are go for printing!";
			Status_Colour.Color = _A.ColourHash(status_green);
			//Status_Colour.Color = _A.ColourHSLA(2, 1, 1);
			_A.Disable_BorderButton(Connect_Button);
			_A.Enable_BorderButton(Disconnect_Button);
			_A.Disable_BorderButton(Home_Button);
			_A.Enable_BorderButton(Print_Button);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Does two things after user-specified synchronisation delays:
		// 1. Sends a signal to the Arduino to move its arm along its entire user-specified length
		// 2. After the print characters (loaded from preview rows) are sent to the printer, sends a signal to begin the print
		private void Begin_Print(object sender, MouseButtonEventArgs e) {
            // Signal to Arduino: Begin actuation after X miliseconds
            threads_serial.send(3, Convert.ToUInt32(_A.INum(Actuation_Delay) * 100));
            threads_serial.send(4, Convert.ToUInt32(_A.INum(Printing_Space) * 100));
            threads_serial.send(5, Convert.ToUInt32(_A.INum(Printing_Speed) * 100));
            threads_serial.send(2, 0); // Tell the actuator to begin actuation

            System.Threading.Thread.Sleep((int)_A.INum(Printer_Delay) + (int)_A.IfCalc(-justified_signum, PrintOffsetDelayCoefficient * 1000 * _A.INum(Print_Offset) / _A.INum(Printing_Speed))); // Wait some time before telling the printer to begin a print, factoring in the left offset if it's applied
            byte[] msg = { 0x1B, 0x4E, 0x31, 0x04 };
            threads_tcp.send(msg, 0, 4); // Now tell the printer to print
        }
    }
}