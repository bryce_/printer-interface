﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Independent_Printing_Interface
{
    class Serial
    {
    }
    ////////////////////////////////////////////////////////////////////////////////////////////
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Packet
    {
        public uint header; //4bytes
        //public uint seqNum; // 4bytes
        public byte command; // 1
        public uint data; // 4bytes 
        public byte checksum; //1
    }
    ////////////////////////////////////////////////////////////////////////////////////////////
    //global static variables
    public static class Globals
    {
        //public static List<Packet> outGoing;// = new Packet();// = new List<Packet>();
        public static Packet packet;
        public static Thread readThread;
        public static Thread processThread;
        public static bool running;
        public static SerialPort portIn;
        //public static byte[] rxData = new byte[1024];
        public static int size = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Packet));
        public static uint acks = 0;
        public static uint sendCount = 0;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    //threaded funcs
    public class Threads_Serial
    {
        //-------------------------------------------------------------------------------------
        public Queue<Packet> sentPack_list;
        public Queue<Packet> recPack_list;
        public uint sequenceNum = 0;
        public int size;

        // This table comes from Dallas sample code where it is
        // freely reusable, though Copyright (C) 2000 Dallas 
        // Semiconductor Corporation
        static byte[] table =
           {
                 0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126,
                 32, 163, 253, 31, 65, 157, 195, 33, 127, 252, 162,
                 64, 30, 95, 1, 227, 189, 62, 96, 130, 220, 35,
                 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3,
                 128, 222, 60, 98, 190, 224, 2, 92, 223, 129, 99,
                 61, 124, 34, 192, 158, 29, 67, 161, 255, 70, 24,
                 250, 164, 39, 121, 155, 197, 132, 218, 56, 102,
                 229, 187, 89, 7, 219, 133, 103, 57, 186, 228, 6,
                 88, 25, 71, 165, 251, 120, 38, 196, 154, 101, 59,
                 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198,
                 152, 122, 36, 248, 166, 68, 26, 153, 199, 37, 123,
                 58, 100, 134, 216, 91, 5, 231, 185, 140, 210, 48,
                 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113,
                 147, 205, 17, 79, 173, 243, 112, 46, 204, 146,
                 211, 141, 111, 49, 178, 236, 14, 80, 175, 241, 19,
                 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82,
                 176, 238, 50, 108, 142, 208, 83, 13, 239, 177,
                 240, 174, 76, 18, 145, 207, 45, 115, 202, 148, 118,
                 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55,
                 213, 139, 87, 9, 235, 181, 54, 104, 138, 212, 149,
                 203, 41, 119, 244, 170, 72, 22, 233, 183, 85, 11,
                 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246,
                 168, 116, 42, 200, 150, 21, 75, 169, 247, 182, 232,
                 10, 84, 215, 137, 107, 53
              };

        //-------------------------------------------------------------------------------------
        public Threads_Serial()
        {
            size = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Packet));
            Globals.packet = new Packet();
            sentPack_list = new Queue<Packet>();
            recPack_list = new Queue<Packet>();
        }
        //-------------------------------------------------------------------------------------
        public void send(byte cmd, uint dat)
        {
            Packet newpack = new Packet
            {
                header = 0xabcdef06,
                command = cmd,
                data = dat,
                checksum = 5
            };
            int size = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Packet));
            byte[] ptr = new byte[size];
            byte[] temp = new byte[size - 1];
            newpack.header = ReverseBytes(newpack.header);

            ptr = StructureToByteArray(newpack);
            for (int i = 0; i < (size - 1); i++)
            {
                temp[i] = ptr[i];
            }
            newpack.checksum = ComputeChecksum(temp);
            ptr = StructureToByteArray(newpack);
            ComputeChecksum(temp);
            try { Globals.portIn.Write(ptr, 0, size); } catch (Exception) { }
            sentPack_list.Enqueue(newpack);
        }
        //-------------------------------------------------------------------------------------
        public void processThread()
        {
            Packet tempRec = new Packet();
            Packet tempSent = new Packet();
            //MessageBox.Show("process Thread started");
            while (true && Globals.running)
            {
                if (recPack_list.Count != 0)
                {
                    tempRec = recPack_list.Dequeue();
                    //means the system recieved a Not Acknowledge
                    if (tempRec.command == 0xfe)
                    {
                        //MessageBox.Show("packet lost/corrupted");
                        //resend that packet
                        for (int i = 0; i < sentPack_list.Count; i++)
                        {

                            tempSent = sentPack_list.ElementAt(i);
                            string toDisplay = "sent: " + tempSent.command.ToString() + "\n" + "rec: " + tempRec.data.ToString();
                            //MessageBox.Show(toDisplay);
                            if (tempSent.command == tempRec.data)
                            {
                                MessageBox.Show("retransmitting data");
                                byte[] ptr = new byte[size];
                                //tempSent.checksum = 111;
                                ptr = StructureToByteArray(tempSent);
                                try
                                {
                                    Globals.portIn.Write(ptr, 0, size);
                                }
                                catch (Exception) { }
                            }
                        }
                    }
                    else if (tempRec.command == 0xff)
                    {
                        //ack successful
                        for (int i = 0; i < sentPack_list.Count; i++)
                        {
                            tempSent = sentPack_list.ElementAt(i);
                            if (tempSent.command == tempRec.data)
                            {
                                sentPack_list.Dequeue();
                            }
                            //MessageBox.Show("ack successful");
                            Globals.acks++;
                        }
                    }
                }
            }
            try { Globals.portIn.Close(); } catch (Exception) { };
        }
        //-------------------------------------------------------------------------------------
        //MULTI-THREADED FUNC THAT IS CONSTANTLY READING IN SERIAL COMMUNICATIONS
        public void ReadSerial()
        {
            Packet newPack = new Packet();
            try { Globals.portIn.Open(); } catch (Exception) { };
            bool headerfound = false;

            while (true && Globals.running)
            {
                int newByte = 0;
                try
                {
                    try { Globals.portIn.Open(); } catch (Exception) { };
                    int header = 0;
                    do
                    {
                        newByte = Globals.portIn.ReadByte();
                        header <<= 8;
                        header = header | (newByte & 0x000000FF);
                        header &= 0x00FFFFFF;
                        headerfound = true;

                    } while (header != 0x00abcdef); 

                    if (headerfound == true)
                    {
                        byte length = (byte)Globals.portIn.ReadByte();
                        //MessageBox.Show(length.ToString());
                        byte[] ptr = new byte[length + 5];

                        ptr[0] = length;
                        ptr[1] = 0xef;
                        ptr[2] = 0xcd;
                        ptr[3] = 0xab;
                        ptr[4] = (byte)Globals.portIn.ReadByte(); //cmd
                        ptr[5] = (byte)Globals.portIn.ReadByte(); //data
                        ptr[6] = (byte)Globals.portIn.ReadByte(); //data
                        ptr[7] = (byte)Globals.portIn.ReadByte(); //data
                        ptr[8] = (byte)Globals.portIn.ReadByte(); //data
                        ptr[9] = (byte)Globals.portIn.ReadByte(); //checksum

                        ByteArrayToStructure(ptr, ref newPack, 0);
                        recPack_list.Enqueue(newPack);
                        //MessageBox.Show(Globals.packet.data.ToString());
                        //string toDisplay = string.Join(Environment.NewLine, ptr);
                        //MessageBox.Show(toDisplay);
                        headerfound = false;

                    }
                }
                catch (Exception) { }
            }
            //threads over
            try { Globals.portIn.Close(); } catch (Exception) { };
        }
        //-------------------------------------------------------------------------------------
        public uint GetNewSQN()
        {
            return sequenceNum++;
        }
        //-------------------------------------------------------------------------------------
        public UInt32 ReverseBytes(UInt32 value)
        {
            return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 |
                (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24;
        }
        //-------------------------------------------------------------------------------------
        public byte ComputeChecksum(params byte[] bytes)
        {
            byte crc = 0;
            if (bytes != null && bytes.Length > 0)
            {
                foreach (byte b in bytes)
                {
                    crc = table[crc ^ b];
                }
            }
            return crc;
        }
        //-------------------------------------------------------------------------------------
        //take from
        //https://stackoverflow.com/questions/3278827/how-to-convert-a-structure-to-a-byte-array-in-c/3278856#3278856
        public static void ByteArrayToStructure<T>(byte[] bytearray, ref T obj, int index)
        {
            int len = Marshal.SizeOf(obj);
            IntPtr i = Marshal.AllocHGlobal(len);
            Marshal.Copy(bytearray, index, i, len);
            obj = (T)Marshal.PtrToStructure(i, typeof(T));
            Marshal.FreeHGlobal(i);
        }
        //-------------------------------------------------------------------------------------
        public byte[] StructureToByteArray(object obj)
        {
            int len = Marshal.SizeOf(obj);

            byte[] arr = new byte[len];

            IntPtr ptr = Marshal.AllocHGlobal(len);

            Marshal.StructureToPtr(obj, ptr, true);

            Marshal.Copy(ptr, arr, 0, len);

            Marshal.FreeHGlobal(ptr);

            return arr;
        }

    }

}

