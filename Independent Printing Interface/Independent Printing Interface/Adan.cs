﻿using System;
using System.Collections.Generic;
//using System.Drawing; // Allows use of the "Graphics" object class and related features
using System.IO; // Allows text file reading and writing
using System.Linq;
using System.Reflection; // Allows use of finding variables by field name via the handy FieldInfo field
//using System.Runtime.InteropServices; // Allows use of "DllImport" and "Marshal."
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
//using System.Windows.Forms;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Independent_Printing_Interface {
	public class Adan {
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Constants - Global const (instead of #define)

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Variables - Global dynamic
		//public Dictionary<string, Brush> BaseBackgrounds = new Dictionary<string, Brush>(); // Use keys starting with numbers to entirely prevent potentially crossing with object names!
		public Dictionary<string, Brush> BaseBackgrounds = new Dictionary<string, Brush>() { { "", new RadialGradientBrush(Colors.White, (Color) ColorConverter.ConvertFromString("#FF6C6C6C")) } }; // Use keys starting with numbers to entirely prevent potentially crossing with object names!
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//   BASIC CALCULATIONS
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Create a pseudo-pointer for any static quotient-type variable_field.  The format of the pointer is "Tuple<Fieldinfo, Window>", where Fieldinfo is a special type for Reflection algorithms and Window is the class wherein it belongs
		public Tuple<FieldInfo, Window> PtrCrt(Window owner, string variable_name, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.FlattenHierarchy | BindingFlags.IgnoreReturn | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static) {
			return new Tuple<FieldInfo, Window>(owner.GetType().GetField(variable_name, bindingAttr), owner);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Get the quotient from a pseudo-pointer for any static quotient-type variable_field
		public object PtrGet(FieldInfo field, Window owner) { return PtrGet(new Tuple<FieldInfo, Window>(field, owner)); }
		public object PtrGet(Tuple<FieldInfo, Window> pointer) {
			return (pointer.Item1).GetValue(pointer.Item2);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Set the quotient of a pseudo-pointer for any static quotient-type variable_field
		//public void PtrSet(object field, object quotient) { PtrSet(field as FieldInfo, quotient); }
		public void PtrSet(FieldInfo field, Window owner, object value) { PtrSet(new Tuple<FieldInfo, Window>(field, owner), value); }
		public void PtrSet(Tuple<FieldInfo, Window> pointer, object value) {
			(pointer.Item1).SetValue(PtrGet(pointer).GetType(), value);
		}
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Checks whether a variable_field has a property of specified name, and returns true if it does
		public bool IfProperty(object obj, string propertyName) {
			return obj.GetType().GetProperty(propertyName) != null;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Returns an output quotient if the condition is greater than zero, or else returns zero
		// Can also be used to check if something is true and multiply with an outside expression if it is.  e.g. square of 2x2 = 2 * 2*IfCalc(2*2) will behave exactly like max(0, 2*2) and thus only return a positive area
		public double IfCalc(double cond, double output = 1) {
			if (cond > 0) {
				//if (output == 0) { return cond; }
				//else { return output; }
				return output;
			}
			else { return 0; }
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Gets a quotient from a numberbox and returns zero if it's empty or has non-numeric characters in it
		public double INum(object sender) { return INum(sender as TextBox); }
		public double INum(TextBox sender) {
			if (Double.TryParse(sender.Text, out double result)) {
				//return Convert.ToDouble((sender as TextBox).Text);
				return result;
			} else { return 0; }
			//return IfCalc(Math.Abs(Double.Parse(sender.Text)));
			//return Double.Parse(sender.Text);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Remove content from an array based on index
		public object[] ArrayRemoveAt(ref object[] input, params int[] indexes) {
			List<object> output = new List<object>(input);

			if (indexes.Length > 1) {
				int counter = 0;
				foreach (int i in indexes) {
					output.RemoveAt(i - counter);
					++counter;
				}
			} else {
				output.RemoveAt(indexes[0]);
			}

			return output.ToArray();
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Handy self-explanatory Colour functions
		//<> Issue: Any Saturation = 0 & Lightness >= 0.75 results in pure white, instead of a shade of light grey
		public Color ColourHash(string hash) {
			return (Color) ColorConverter.ConvertFromString(hash);
		}

		//public Color ColourRGBA(int R = 0, int G = 0, int B = 0, int Alpha = 255) { return ColourRGBA(R, G, B, Alpha); }
		public Color ColourRGBA(byte R = 0, byte G = 0, byte B = 0, byte A = 255) {
			return new Color() { R = R, G = G, B = B, A = A };
		}

		public Color ColourHSVA(double Hue = 3, double Saturation = 1, double Vibrancy = 1, byte Alpha = 255) {
			Saturation = Math.Max(0, Math.Min(1, Saturation)); // Limit Saturation between 0 and 1
			Vibrancy = Math.Max(0, Math.Min(1, Vibrancy)); // Limit Lightness between 0 and 1
			Hue = (Hue + 1) % 3; // Remove 'unnecessary revolutions' from the Hue quotient and do number magic, so that... 0 = Blue, 0.5 = Purple, 1 = Red, 1.5 = Yellow, 2 = Green, 2.5 = Cyan, 2.99 = near-Blue
			byte ColCode() {
				double quotient = IfCalc(-1*(Hue - 2), Math.Min(1, 2*(1 - Math.Abs(Hue - 1)))); // Only use Hue if it's between 0 and 2, with 1 being 100% intensity of colour
				quotient = (1 - Saturation) + Saturation * quotient;
				quotient = Vibrancy * quotient;
				Hue = (Hue + 2) % 3; // 1 = Red --> 1 = Green --> 1 = Blue
				return (byte) (255 * quotient);
			}
			return new Color() { R = ColCode(), G = ColCode(), B = ColCode(), A = Alpha };
		}

		public Color ColourHSLA(double Hue = 3, double Saturation = 1, double Lightness = 0.5, byte Alpha = 255) {
			Saturation = Math.Max(0, Math.Min(1, Saturation)); // Limit Saturation between 0 and 1
			Lightness = Math.Max(0, Math.Min(1, Lightness)); // Limit Lightness between 0 and 1
			Hue = (Hue + 1) % 3; // Remove 'unnecessary revolutions' from the Hue quotient and do number magic, so that... 0 = Blue, 0.5 = Purple, 1 = Red, 1.5 = Yellow, 2 = Green, 2.5 = Cyan, 2.99 = near-Blue
			byte ColCode() {
				double quotient = IfCalc(-1*(Hue - 2), Math.Min(1, 2*(1 - Math.Abs(Hue - 1)))); // Only use Hue if it's between 0 and 2, with 1 being 100% intensity of colour
				quotient = 0.5*(1 - Saturation) + Saturation * quotient;
				quotient = Math.Min(1, quotient * 2*Lightness + Math.Max(0, 2*Lightness - 1));
				Hue = (Hue + 2) % 3; // 1 = Red --> 1 = Green --> 1 = Blue
				return (byte) (255 * quotient);
			}
			return new Color() { R = ColCode(), G = ColCode(), B = ColCode(), A = Alpha };
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Gets the string name of a type input to the function
		public string StringType(object type) {
			return type.ToString();
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//   USER INTERFACE
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Generates or gets the new_window and waits for the user to click cancel or confirm before exiting the function, and returns all public static variables
		public object Window_Waiter(Window owner, object window, string variable_name) { return Window_Waiter(owner, window, variable_name, "")[0]; }
			//object result = Window_Waiter(window, variable_name, "")[0]; MessageBox.Show(Convert.ToString(result.GetType())); MessageBox.Show(Convert.ToString(result)); return result; }
		public object[] Window_Waiter(Window owner, object new_window, string first_variable_name = "", params string[] variable_names) {
			(new_window as Window).Owner = owner; // Make this new_window the owner of the new new_window
			(new_window as Window).ShowDialog(); // Open the new_window and wait until it's closed

			List<object> result = new List<object>();

			if (first_variable_name != "") { // We want to return only certain variables from the data, specified by name
				result.Add((new_window as Window).GetType().GetField(first_variable_name).GetValue(this));
				foreach (string name in variable_names) {
					if (name != "") {
						result.Add((new_window as Window).GetType().GetField(name).GetValue(this));
					}
				}
			} else {
				List<FieldInfo> public_static_fields = new List<FieldInfo>((new_window as Window).GetType().GetFields()); // Get a list of all variables from the other new_window's public static data
				foreach(FieldInfo variable_field in public_static_fields) { // Get each variable_field's actual quotient
					result.Add(variable_field.GetValue(this));
				}
			}

			return result.ToArray();
		}
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Determine which ListBoxItem is focused, out of those chosen to check
		public ListBoxItem Focused_ListBoxItem(params ItemCollection[] collections) {
			foreach (ItemCollection collection in collections) {
				foreach (ListBoxItem item in collection) {
					if (item.IsFocused) {
						return item;
					}
				}
			}
			return null;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Create a list of type, including all children of an element, based on the specified type(s) and potentially the GroupName
		public List<object> List_Children(object parent, params Type[] type_is) { return List_Children("", parent, type_is); }
		public List<object> List_Children(string group_name, object parent, params Type[] type_is) {
			List<object> result = new List<object>();

			foreach (object element in (parent as Grid).Children) {
				if (type_is.Contains(element.GetType())) {
					if (group_name == "" || (IfProperty(element, "GroupName") && group_name == (element as RadioButton).GroupName)) {
						result.Add(element);
					}
				}
			}

			return result;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Find the activated item within a list, based on its nature.  Returns after the first activated item is found
		// Pairs with the List_Children function
		public object Find_Activated(List<object> elements) {
			foreach (object element in elements) {
				if (element.GetType() == typeof(RadioButton) && ((element as RadioButton).IsChecked ?? false)) {
					return element;
				}
			}
			return new Control { Name = "" }; // Nothing is 'activated'
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Activates the item within a list of specified name, based on its nature
		// Pairs with the List_Children function
		public void Do_Activate(List<object> elements, string name) {
			foreach (object element in elements) {
				if ((element as Control).Name == name) {
					if (element.GetType() == typeof(RadioButton)) {
						(element as RadioButton).IsChecked = true;
					}
					return;
				}
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// High-traffic use of Brush.Background setters and getters
		public Brush TryGetReturnDictBrush(Dictionary<string, Brush> dict, string inkey, Brush no_brush = default(Brush)) {
			if (dict.TryGetValue(inkey, out Brush temp_brush)) {
				return temp_brush;
			} else {
				return no_brush;
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Disables the specified border button and changes the gradient background colours to match
		public void Disable_BorderButton(object sender, Brush back = default(Brush)) { Disable_BorderButton(sender as Border, back); }
		public void Disable_BorderButton(Border sender, Brush back = default(Brush)) {
			sender.IsEnabled = false;
			if (!BaseBackgrounds.ContainsKey(sender.Name)) { // Add the sender's background to the BaseBackgrounds dictionary if it's not yet there
				BaseBackgrounds.Add(sender.Name, sender.Background);
			} else if (sender.Background != BaseBackgrounds[""]) { // Replace the existing background in the baseBackgrounds dictionary
				BaseBackgrounds[sender.Name] = sender.Background;
			}
			sender.Background = TryGetReturnDictBrush(BaseBackgrounds, "");
		}

		public void Disable_BorderButton(object sender, string inkey) { Disable_BorderButton(sender as Border, inkey); }
		public void Disable_BorderButton(Border sender, string inkey) {
			Disable_BorderButton(sender, TryGetReturnDictBrush(BaseBackgrounds, inkey));
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Enables the specified border button and changes the gradient background colours to match
		public void Enable_BorderButton(object sender, Brush back = default(Brush)) { Enable_BorderButton(sender as Border, back); }
		public void Enable_BorderButton(Border sender, Brush back = default(Brush)) {
			sender.IsEnabled = true;
			if (back == default(Brush)) {
				if (BaseBackgrounds.TryGetValue(sender.Name, out back)) { // Only revert to previous colour if the colour is registered
					sender.Background = back;
				}
			}
		}

		public void Enable_BorderButton(object sender, string inkey) { Disable_BorderButton(sender as Border, inkey); }
		public void Enable_BorderButton(Border sender, string inkey) {
			Enable_BorderButton(sender, TryGetReturnDictBrush(BaseBackgrounds, inkey));
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//   FILE OPERATIONS
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Create a list based on the parameters input to the function, used for save_data and load_data
		public List<object> Create_DataList(params object[] storage) {
			List<object> result = new List<object>();
			foreach(object element in storage) {
				result.Add(element);
			}

			return result;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Create a Tuple<string,object,string> of standard type that is parsed by the saving / loading system, for the purpose of adding comments as a prefix or suffix to saved data
		public Tuple<string, object, string> Data(object data, string suffix = "") { return Data("", data, suffix); }
		public Tuple<string, object, string> Data(string prefix, object data, string suffix = "") {
			return new Tuple<string, object, string>(prefix, data, suffix);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Save data from an object list into a chosen text file.  i.e. File = Data
		// NB: Does not require that all elements in the list are reference-type objects, e.g. both work, string[] (array) and string (quotient)
		public void Save_Data(string address, object storage) { Save_Data(address, storage as List<object>); }
		public void Save_Data(string address, List<object> storage) {
			if (!storage.Any()) { return; } // Immediately end the function if there is no data to read or write
			string prefix = "", suffix = "";
			using (StreamWriter stream = File.CreateText(address)) {
				foreach (object element in storage) {
					if (element == null) { // Write a line of nothing
						stream.WriteLine();
					} else {
						Recursive_Save(element);
					}
				}

				// Finally, save the data
				void Save(object data) {
					stream.WriteLine(prefix + Convert.ToString(data) + suffix);
				}

				// Recursively get the data, preserving object reference.  If an item is not the intended target, the function repeats
				void Recursive_Save(object element) {
					switch (element.GetType().ToString()) { // Treat each different type of object based on its nature of usage
						case "System.Tuple`3[System.String,System.Object,System.String]": prefix = (element as Tuple<string, object, string>).Item1; suffix = (element as Tuple<string, object, string>).Item3; Recursive_Save((element as Tuple<string, object, string>).Item2); prefix = ""; suffix = ""; break;
						//case "System.Reflection.RtFieldInfo": Recursive_Save(PtrGet(element as FieldInfo)); break;
						case "System.Tuple`2[System.Reflection.FieldInfo,System.Windows.Window]": Recursive_Save(PtrGet(element as Tuple<FieldInfo, Window>)); break;
						case "System.String": Save(element as string); break;
						case "System.Double": Save(Convert.ToDouble(element)); break;
						case "System.Int32": Save(Convert.ToInt32(element)); break;
						case "System.String[]": Save((element as string[])[0]); break;
						case "System.Double[]": Save((element as double[])[0]); break;
						case "System.Int32[]": Save((element as int[])[0]); break;
						case "System.Windows.Controls.TextBox": case "Independent_Printing_Interface.NumberBox": Save((element as TextBox).Text); break;
						case "System.Windows.Controls.Slider": Save((element as Slider).Value); break; //<> Untested
						case "System.Collections.Generic.List`1[System.Object]": Save((Find_Activated(element as List<object>) as Control).Name); break;
						default:
							Console.Write("Unrecognised type in data saving:   " + element.GetType().ToString());
							MessageBox.Show("Unrecognised type in data saving:\n" + element.GetType().ToString() + "\n\n This type-string has been copied to clipboard.");
							Clipboard.SetText(element.GetType().ToString());
							stream.WriteLine(); break; // Do nothing with an unrecognised type
					}
				}
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Load data into an object list from a chosen text file.  i.e. Data = File
		// NB: Requires that all elements in the list are reference-type objects, e.g. string[] (array) only works instead of string (quotient)
		public void Load_Data(string address, object storage) { Load_Data(address, storage as List<object>); }
		public void Load_Data(string address, List<object> storage) {
			if (!storage.Any()) { return; } // Immediately end the function if there is no data to read or write
			string prefix = "", suffix = "";
			using (StreamReader stream = File.OpenText(address)) {
				foreach (object element in storage) {
					if (element == null || element.GetType() == typeof(string)) { // Skip the line without data
						stream.ReadLine();
					} else {
						Recursive_Load(element);
					}
				}

				// Finally, load the data
				string Load() {
					string output = stream.ReadLine().Remove(0, prefix.Length); // Read the line and remove the prefix
					output = output.Substring(0,output.Length - suffix.Length); // Remove the suffix
					return output;
				}

				// Recursively get the data, preserving object reference.  If an item is not the intended target, the function repeats
				void Recursive_Load(object element) {
					switch (element.GetType().ToString()) { // Treat each different type of object based on its nature of usage
						case "System.Tuple`3[System.String,System.Object,System.String]": prefix = (element as Tuple<string, object, string>).Item1; suffix = (element as Tuple<string, object, string>).Item3; Recursive_Load((element as Tuple<string, object, string>).Item2); prefix = ""; suffix = ""; break;
						case "System.Tuple`2[System.Reflection.FieldInfo,System.Windows.Window]": Tuple<FieldInfo, Window> pointer = element as Tuple<FieldInfo, Window>;
							switch (PtrGet(pointer).GetType().ToString()) {
								case "System.String": PtrSet(pointer, Load()); break;
								case "System.Double": PtrSet(pointer, Convert.ToDouble(Load())); break;
								case "System.Int32": PtrSet(pointer, Convert.ToInt32(Load())); break;
							} break;
						case "System.String[]": (element as string[])[0] = stream.ReadLine(); break;
						//case "System.String[]": Recursive_Load((element as object[])[0], stream); break; //<> Untested
						case "System.Double[]": (element as double[])[0] = Convert.ToDouble(Load()); break;
						//case "System.Double[]": Recursive_Load((element as object[])[0], stream); break; //<> Untested
						case "System.Int32[]": (element as int[])[0] = Convert.ToInt32(Load()); break;
						//case "System.Int32[]": Recursive_Load((element as object[])[0], stream); break; //<> Untested
						case "System.Windows.Controls.TextBox": case "Independent_Printing_Interface.NumberBox": (element as TextBox).Text = Load(); break;
						case "System.Windows.Controls.Slider": (element as Slider).Value = Convert.ToDouble(Load()); break; //<> Untested
						case "System.Collections.Generic.List`1[System.Object]": Do_Activate(element as List<object>, Load()); break;
						default:
							Console.Write("Unrecognised type in data loading:   " + element.GetType().ToString());
							MessageBox.Show("Unrecognised type in data loading:\n" + element.GetType().ToString() + "\n\n This type-string has been copied to clipboard.");
							Clipboard.SetText(element.GetType().ToString());
							Load(); break; // Do nothing with an unrecognised type
					}
				}
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Delete data and remove a chosen file.  i.e. File = nothing
		public void Delete_Data(string address) {
			if (File.Exists(address)) {
				File.Delete(address);
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Prevent writing non-numeric characters into a NumberBox
	public class NumberBox:TextBox {
		private bool integerOnly = false;
		public bool IntegerOnly { get { return integerOnly;} set { integerOnly=value; } }
		private int decimalPlaces = 2;
		public int DecimalPlaces { get { return decimalPlaces;} set { decimalPlaces=value; } }
		private double increment = 1;
		public double Increment { get { return increment;} set { increment=value; } }
		private double maxValue = Math.Pow(10, 999); // Essentially infinite
		public double MaxValue { get { return maxValue;} set { maxValue=value; } }
		private double minValue = 0;
		public double MinValue { get { return minValue;} set { minValue=value; } }

		public NumberBox() {
			MaxLines = 1;
			TextWrapping = TextWrapping.NoWrap;
			TextAlignment = TextAlignment.Center;
			
			AddHandler(TextChangedEvent, 
				new RoutedEventHandler(Preserve_Bounds), true);
			AddHandler(PreviewTextInputEvent,
				new RoutedEventHandler(Validate), true);
			AddHandler(KeyDownEvent,
				new RoutedEventHandler(Increment_Value), true);
		}

		// Preserve the upper and lower bounds of the value, and preserve the 
		private void Preserve_Bounds(object sender, RoutedEventArgs e) {
			int caret = SelectionStart;
			double text = Convert.ToDouble(Text);
			if (text > maxValue) { Text = Convert.ToString(maxValue); }
			else if (text < minValue) { Text = Convert.ToString(minValue); }
			if (text != Math.Round(text, decimalPlaces)) { Text = Convert.ToString(Math.Round(text, decimalPlaces)); }
			SelectionStart = caret;
		}

		// Ensure only numeric keys are passed into the box
		private void Validate(object sender, RoutedEventArgs e) {
			Regex regex;
			if (Text.Contains(".") || integerOnly == true) {
				regex = new Regex("[^0-9]");
			} else {
				regex = new Regex("[^0-9].");
			}
			e.Handled = regex.IsMatch((e as TextCompositionEventArgs).Text);
		}

		// When up/down arrow keys are pressed, increment the value inside
		private void Increment_Value(object sender, RoutedEventArgs e) {
			if ((e as KeyEventArgs).Key == Key.Up ) {
				Text = Convert.ToString(Math.Min(maxValue, Convert.ToDouble(Text) + increment));
			} else if ((e as KeyEventArgs).Key == Key.Down ) {
				Text = Convert.ToString(Math.Max(minValue, Convert.ToDouble(Text) - increment));
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Highlight entire TextBox upon click, used primarily for copy-paste text boxes
	public class HighlightingBox:TextBox {
		public HighlightingBox() {
			AddHandler(PreviewMouseLeftButtonDownEvent,
				new MouseButtonEventHandler(SelectivelyIgnoreMouseButton), true);
			AddHandler(GotKeyboardFocusEvent,
				new RoutedEventHandler(SelectAllText), true);
			AddHandler(MouseDoubleClickEvent,
				new RoutedEventHandler(SelectAllText), true);
		}

		private static void SelectivelyIgnoreMouseButton(object sender, MouseButtonEventArgs e) {
			// Find the TextBox
			DependencyObject parent = e.OriginalSource as UIElement;
			while (parent != null && !(parent is TextBox))
				parent = VisualTreeHelper.GetParent(parent);

			if (parent != null) {
				var textBox = (TextBox) parent;
				if (!textBox.IsKeyboardFocusWithin) {
					// If the text box is not yet focussed, give it the focus and
					// stop further processing of this click event.
					textBox.Focus();
					e.Handled = true;
				}
			}
		}

		private static void SelectAllText(object sender, RoutedEventArgs e) {
			var textBox = e.OriginalSource as TextBox;
			if (textBox != null)
				textBox.SelectAll();
		}
	}
}