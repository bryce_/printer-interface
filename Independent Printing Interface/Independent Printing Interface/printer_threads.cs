﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;


namespace Independent_Printing_Interface
{
    class printer_threads
    {
    }
    public class Threads_TCP
    {
        //class vars
        public MyTCP tcp;
        public Thread receive;
        public Thread process;
        public bool running = false;
        public Byte[] bytes;
        public string data = null;
        //-----------------------------------
        public Threads_TCP()
        {
            bytes = new Byte[256];
            tcp = new MyTCP("169.254.181.50", 7000);
        }
        //-----------------------------------
        // THREADED: recieving data
        //-----------------------------------
        public void rec()
        {
            int i = 0;
            int temp = 0;
            //tcp.ServerStart();
            while (true && running)
            {
                if (tcp.stream.CanRead)
                {
                    try { i = tcp.stream.Read(bytes, 0, 256); } catch (Exception) { }
                    //temp = bytes;
                    data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                    //for (int j = 0; j < 4; j++)
                    //{
                    //    temp <<= 8;
                    //    int cheese = bytes[j];
                    //    temp = temp | cheese;

                    //}
                    //MessageBox.Show(data);
                }
            }
        }
        //-----------------------------------
        // THREADED: Sorts incoming data
        //-----------------------------------
        public void proccess()
        {
            while (true && running)
            {

            }
        }
        //-----------------------------------
        // SEND
        //-----------------------------------
        public void send(byte[] buf, int offset, int size)
        {
            if (running)
                tcp.stream.Write(buf, offset, size);
        }
    }
    public class MyTCP
    {
        //class vars
        public TcpListener server = null;
        public Int32 currPort;
        public string currIP;
        public NetworkStream stream;
        public TcpClient client;
        //----------

        //class constructor
        public MyTCP(string ip, Int32 port)
        {
            currPort = port;
            currIP = ip;
            IPAddress IP = IPAddress.Parse(ip);//("192.168.127.254");
        }

        //methods
        public int ServerStart()
        {
            client = new TcpClient();//("192.168.127.254", 7000);
            client.Connect(currIP, currPort);
            stream = client.GetStream();
            return 0;
        }

    }
}
