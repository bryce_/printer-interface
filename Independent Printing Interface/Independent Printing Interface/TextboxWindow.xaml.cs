﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Independent_Printing_Interface {
	public partial class TextboxWindow:Window {
		public static string text_input;

		public TextboxWindow(string message) {
			InitializeComponent();
			text_input = null; // Reset the value of the text box
			MessageBlock.Text = message;
			InputBox.Focus();
		}

		private void Confirm(object sender = null, RoutedEventArgs e = null) {
			text_input = InputBox.Text;
			this.Close();
		}

		private void Cancel(object sender = null, RoutedEventArgs e = null) {
			this.Close();
		}

		private void OnKeyDownHandler(object sender, KeyEventArgs e) {
			if (e.Key == Key.Return) { Confirm(); }
			else if (e.Key == Key.Escape) { Cancel(); }
		}
	}
}
